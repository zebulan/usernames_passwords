from os import path
from time import time


def create_file(filename, source):
    """ Create a text file from source list with given filename """
    total_lines = 0
    with open(filename, 'w') as file:
        for row in sorted(source):
            file.write(''.join((row, '\n')))
            total_lines += 1
    return total_lines


def check_exist(filename):
    if path.exists(filename):
        while True:
            overwrite = input('{} exists.\nOverwrite? (Y/N) '.format(filename))
            if overwrite.lower() == 'y':
                return True  # ?
            elif overwrite.lower() == 'n':
                return False


def separate_user_pass():
    """ Create two text files. One for unique usernames and
        one for unique passwords. """

    u_filename = 'list_of_usernames.txt'
    p_filename = 'list_of_passwords.txt'
    if not check_exist(u_filename):
        return
    elif check_exist(p_filename):
        return

    usernames = set()
    passwords = set()
    with open('10-million-combos.txt', 'r') as f:
        for line in f:
            try:
                username, password = line.rstrip().split()
                usernames.add(username)
                passwords.add(password)
            except ValueError:
                usernames.add(line.rstrip())

    num_u = create_file(u_filename, usernames)
    num_p = create_file(p_filename, passwords)
    if num_u and num_p:
        print('{} usernames.\n{} passwords.'.format(num_u, num_p))


def combine_user_pass():
    """ Create a single text file of combining unique
        usernames and passwords. """
    both_filename = 'user-pass_combined.txt'
    if not check_exist(both_filename):
        return

    user_pass = set()
    with open('10-million-combos.txt', 'r') as f:
        for line in f:
            for each in line.rstrip().split():
                user_pass.add(each)

    total_lines = create_file('user-pass_combined.txt', user_pass)
    if total_lines:
        print('{} unique usernames/passwords.'.format(total_lines))

if __name__ == '__main__':
    while True:
        answer = input('One or Two files? (1/2) ')
        if answer == '1' or answer.lower() == 'one':
            start = time()
            combine_user_pass()
            break
        elif answer == '2' or answer.lower() == 'two':
            start = time()
            separate_user_pass()
            break
    print('Elapsed Time: {:.2f} seconds.'.format(time() - start))
